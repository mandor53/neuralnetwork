# MediOCR

MediOCR is a next generation, high performances OCR made by EPITA students.

## Installation

### Git

```
# git clone https://github.com/Artemiche/mediocr.git
# make all
```

## Usage

### Overview

As of now, MediOCR is a very simple, but yet flexible, Neural Network able to perform XOR operations.

### Execution

```
# ./main
```
