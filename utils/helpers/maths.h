
#include "../types/arrays.h"

#ifndef UTILS_HELPERS_MATHS_
#define UTILS_HELPERS_MATHS_

double maths_rand(double a, double b);
double maths_sigmoid(double x);
double maths_dsigmoid(double y);

#endif
